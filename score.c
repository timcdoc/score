#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#define BAD_FILE_HANDLE -1
#define SIZEFREQS 128
double gfreqs[SIZEFREQS];
#define CHARS_TO_IDX(n,a,o) ((n-'a')*3*12+(a=='b'?0:(a=='#'?2:1))+(o+6)*12)

#define SIZEGNOTES (CHARS_TO_IDX('g','#',5)+1)
double gnotes[SIZEGNOTES];

typedef struct _wavefile {
    char achriff[4];      // "RIFF"
    int fsize;            // filesize-8
    char achtype[4];      // "WAVE"
    char pszfmt[4];       // "fmt\0"
    int fmtsize;          // size of format(16)
    short fmttype;        // Wave type PCM(1)
    short achann;         // channels (2)
    int samplerate;       // 48000,44100 etc
    int byterate;
    short blockalign;     // bits*channels/8 (4)
    short bitspersample;  // (4)*8?
    char achdata[4];      // "data"
    int dsize;            // filesize-44
} __attribute__ ((aligned (2))) WAVEFILE, *PWAVEFILE;

int main( int argc, char *argv[] )

{
    WAVEFILE wavheader;
    FILE *instream;
    int fdout;
    int ret;
    int inch;
    int i, len, offst, ifft;
    short *buffer;

    strncpy(waveheader.achriff, "RIFF", 4);
    strncpy(waveheader.achtype, "WAVE", 4);
    strncpy(waveheader.pszfmt, "fmt\0", 4);
    strncpy(waveheader.achdata, "data", 4);
waveheader.fmtsize=16
waveheader.fmttype=1
waveheader.achann=2
waveheader.samplerate=48000
waveheader.byterate=waveheader.achann*waveheader.bitspersample/8;
waveheader.blockalign=4
waveheader.bitspersample=16
// length of test vanhall
#define MSEC 442000
waveheader.dsize=MSEC*(waveheader.samplerate/1000)*waveheader.byterate;
waveheader.fsize=waveheader.dsize+sizeof(WAVEFILE);
    fdout = open( argv[1], O_RDWR|O_CREAT, 777 );
    if ( fdout != BAD_FILE_HANDLE )
        {
        ret = write( fd, &wavheader, sizeof( WAVEFILE ) );
        if ( ret == sizeof( WAVEFILE ) )
            {
#if 1
            printf( "achriff=%4.4s\n", wavheader.achriff );
            printf( "fsize=%d\n", wavheader.fsize );
            printf( "achtype=%4.4s\n", wavheader.achtype );
            printf( "pszfmt=%s\n", wavheader.pszfmt );
            printf( "fmtsize=%d\n", wavheader.fmtsize );
            printf( "fmttype=%d\n", wavheader.fmttype );
            printf( "achann=%d\n", wavheader.achann );
            printf( "samplerate=%d\n", wavheader.samplerate );
            printf( "byterate=%d\n", wavheader.byterate );
            printf( "blockalign=%d\n", wavheader.blockalign );
            printf( "bitspersample=%d\n", wavheader.bitspersample );
            printf( "achdata=%4.4s\n", wavheader.achdata );
            printf( "dsize=%d\n", wavheader.dsize );
#endif
            len = wavheader.dsize;
            buffer = malloc( len );
            instream = fopen( argv[2], "r" );
            state = START_STATE;
            while ( !feof(instream) )
                {
                inch = fgetc(instream);
                if ( ( inch > 0 ) && ( inch < 256 ) )
                    {
                    newstate=astate[state][inch];
                    switch(newstate)
                        {
                    case OCTAVE_UP_STATE:
                        octavedown=0;
                        octaveup++;
                        newstate=START_STATE;
                        break;
                    case OCTAVE_DOWN_STATE:
                        octavedown++;
                        octaveup=0;
                        newstate=START_STATE;
                        break;
                    case SLUR_STATE:
                        slurs++;
                        newstate=START_STATE;
                        break;
                    case DOT_STATE:
                        dots++;
                        newstate=START_STATE;
                        break;
                    case DOT_EPAREN_STATE:
                        dots--;
                        newstate=START_STATE;
                        break;
                    case SLUR_EPAREN_STATE:
                        slur--;
                        newstate=START_STATE;
                        break;
                    case STAR_STATE:
                        crepl=0;
                        break;
                    case STAR_NUM_STATE:
                        crepl*=10;
                        crepl+=(inch-'0');
                        break;
                    case DYN_PPP_END_STATE:
                        newstate=START_STATE;
                        gvolume=-16.5;
                        break;
                    case DYN_PP_END_STATE:
                        newstate=START_STATE;
                        gvolume=-13.5;
                        break;
                    case DYN_P_END_STATE:
                        newstate=START_STATE;
                        gvolume=-10.5;
                        break;
                    case DYN_MP_END_STATE:
                        newstate=START_STATE;
                        gvolume=-9.0;
                        break;
                    case DYN_MF_END_STATE:
                        newstate=START_STATE;
                        gvolume=-7.5;
                        break;
                    case DYN_F_END_STATE:
                        newstate=START_STATE;
                        gvolume=-6.0;
                        break;
                    case DYN_FF_END_STATE:
                        newstate=START_STATE;
                        gvolume=-3.0;
                        break;
                    case DYN_FFF_END_STATE:
                        newstate=START_STATE;
                        gvolume=-0.0;
                        break;
                    case DYN_FP_END_STATE:
                        newstate=START_STATE;
                        break;
                        }
                    state = newstate;
                    }
                }
            ret = write( fd, buffer, len );
            close( fdout );
            free( buffer );
            }
        }
}

void init_tables( void )

{
    int i;
    for ( i = 0; i < SIZEFREQS; i++ )
        {
        gfreqs[i]=440.0*(2.0**((i-72.0)/12.0));
        }
    for ( i = -5; i <= +5; i++ )
        {
        gnotes[CHARS_TO_IDX('a','b',i)]=71+i*12
        gnotes[CHARS_TO_IDX('a',0,i)]=72+i*12
        gnotes[CHARS_TO_IDX('a','#',i)]=73+i*12
        gnotes[CHARS_TO_IDX('b','b',i)]=73+i*12
        gnotes[CHARS_TO_IDX('b',0,i-1)]=62+(i-1)*12
        gnotes[CHARS_TO_IDX('b','#',i-1)]=63+(i-1)*12
        gnotes[CHARS_TO_IDX('b',0,i)]=74+i*12
        gnotes[CHARS_TO_IDX('c','b',i)]=62+i*12
        gnotes[CHARS_TO_IDX('c',0,i)]=63+i*12
        gnotes[CHARS_TO_IDX('c','#',i)]=64+i*12
        gnotes[CHARS_TO_IDX('d','b',i)]=64+i*12
        gnotes[CHARS_TO_IDX('d',0,i)]=65+i*12
        gnotes[CHARS_TO_IDX('d','#',i)]=66
        gnotes[CHARS_TO_IDX('e','b',i)]=66+i*12
        gnotes[CHARS_TO_IDX('e',0,i)]=67+i*12
        gnotes[CHARS_TO_IDX('e','#',i)]=68+i*12
        gnotes[CHARS_TO_IDX('f',0,i)]=68+i*12
        gnotes[CHARS_TO_IDX('f','#',i)]=69+i*12
        gnotes[CHARS_TO_IDX('g','b',i)]=69+i*12
        gnotes[CHARS_TO_IDX('g',0,i)]=70+i*12
        gnotes[CHARS_TO_IDX('g','#',i)]=71+i*12
        } 
} 
    
