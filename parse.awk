BEGIN {
    for ( i = 0; i < 128; i++ ) {
        freqs[i]=440.0*(2.0**((i-72.0)/12.0))
    }
    for ( o = -5; o <= +5; o++ ) {
        notes["ab",o]=71+o*12
        notes["a",o]=72+o*12
        notes["a#",o]=73+o*12
        notes["bb",o]=73+o*12
        notes["b",o-1]=62+(o-1)*12
        notes["b#",o-1]=63+(o-1)*12
        notes["b",o]=74+o*12
        notes["cb",o]=62+o*12
        notes["c",o]=63+o*12
        notes["c#",o]=64+o*12
        notes["db",o]=64+o*12
        notes["d",o]=65+o*12
        notes["d#",o]=66
        notes["eb",o]=66+o*12
        notes["e",o]=67+o*12
        notes["e#",o]=68+o*12
        notes["f",o]=68+o*12
        notes["f#",o]=69+o*12
        notes["gb",o]=69+o*12
        notes["g",o]=70+o*12
        notes["g#",o]=71+o*12
    } 
    key["2b", "a"]="a"
    key["2b", "b"]="bb"
    key["2b", "c"]="c"
    key["2b", "d"]="d"
    key["2b", "e"]="eb"
    key["2b", "f"]="f"
    key["2b", "g"]="g"

    key["3b", "a"]="ab"
    key["3b", "b"]="bb"
    key["3b", "c"]="c"
    key["3b", "d"]="d"
    key["3b", "e"]="eb"
    key["3b", "f"]="f"
    key["3b", "g"]="g"
    octave=0
    lastn=-1
    aspeed[2]=0.125
    aspeed[3]=0.25
    aspeed[4]=0.5
    aspeed[5]=1.0
    aspeed[6]=2.0
    aspeed[7]=4.0
    avolume["pp"]=-30.0
    avolume["p"]=-20.0
    avolume["fp"]=-20.0
    avolume["f"]=-10.0
    avolume["ff"]=0.0
    volume=-10.0
    crepl=1
} {
    cline++
    printf( "line=%d\n", cline )
    for ( i = 1; i <= NF; i++ ) {
        if ( $i == "+" ) {
            octavedown=0
            octaveup++
        } else if ( $i ~ /^\*[1-9][0-9]*$/ ) {
            crepl=$i
            sub( /^\*/, "", crepl )
            crepl = 0+crepl
        } else if ( $i == "-" ) {
            octaveup=0
            octavedown++
        } else if ( $i == "(dyn" ) {
            j=i+1
            volume=avolume[$j]
            i+=2
        } else if ( $i == "(s" ) {
            slurs++
        } else if ( $i == "s)" ) {
            slurs--
        } else if ( $i == "(." ) {
            dots++
        } else if ( $i == ".)" ) {
            dots--
        } else if ( $i ~ /^[234567]$/ ) {
            speed=aspeed[$i]
            speeddots=0
        } else if ( $i == "." ) {
            speeddots++
        } else if ( $i == "r" ) {
            while ( crepl > 0 ) {
                if ( speeddots > 0 ) {
                    sstr = sprintf( "play -r 48000 -n -b 32 -c 2 trim 0.0 %6.5f", 0.75*speed*(2**speeddots-1)/(2**speeddots), freqs[n] )
                    ctime += 0.75*speed*(2**speeddots-1)/(2**speeddots)
                } else {
                    sstr = sprintf( "play -r 48000 -n -b 32 -c 2 trim 0.0 %6.5f", 0.75*speed )
                    ctime += 0.75*speed
                }
                speeddots=0
                #system(sstr)
                crepl--
            }
            crepl=1
        } else if ( $i ~ /^[a-g][nb#]*$/ ) {
            while ( crepl > 0 ) {
            if ( length($i) == 1 ) {
                n = notes[key["3b", $i ], octave ]
                nu = notes[key["3b", $i ], octave+1 ]
                nd = notes[key["3b", $i ], octave-1 ]
            } else if ( $i ~ /^[a-g]n/ ) {
                n = notes[substr($i,1,1), octave ]
                nu = notes[substr($i,1,1), octave+1 ]
                nd = notes[substr($i,1,1), octave-1 ]
            } else {
                n = notes[$i, octave ]
                nu = notes[$i, octave+1 ]
                nd = notes[$i, octave-1 ]
            }
            if ( lastn > -1 ) {
                printf( "lastn=%d, nd=%d, n=%d, nu=%d\n", lastn, nd, n, nu )
                mycase = 0
                if ( abs(lastn-n) < abs(lastn-nu) ) {
                    if ( abs(lastn-n) > abs(lastn-nd) ) {
                        mycase = -1
                    }
                } else {
                    if ( abs(lastn-nu) < abs(lastn-nd) ) {
                        mycase = +1
                    } else {
                        mycase = -1
                    }
                }
            }
            octave += ( octaveup + mycase - octavedown )

            if ( length($i) == 1 ) {
                n = notes[key["3b", $i ], octave ]
            } else if ( $i ~ /^[a-g]n/ ) {
                n = notes[substr($i,1,1), octave ]
            } else {
                n = notes[$i, octave ]
            }
            printf( "n=%d, notes=%s, octave=%d freq=%6.5f\n", n, $i, octave, freqs[n] )
#sstr = sprintf( "play -V -r 48000 -n -b 32 -c 2 synth %6.5f sin %6.5f vol -10dB", 0.75*speed*(2**speeddots-1)/(2**speeddots), freqs[n] )
            if ( speeddots > 0 ) {
                totlen = 0.75*speed*(2**speeddots-1)/(2**speeddots)
            } else {
                totlen = 0.75*speed
            }
            if ( slurs > 0 ) {
                sstr = sprintf( "play -r 48000 -n -b 32 -c 2 synth %6.5f sin %6.5f vol %6.5fdB", totlen, freqs[n], volume )
                ctime += totlen
                #system(sstr)
            } else if ( dots > 0 ) {
                sstr = sprintf( "play -r 48000 -n -b 32 -c 2 synth %6.5f sin %6.5f vol %6.5fdB", totlen*0.5, freqs[n], volume+5.0 )
                ctime += totlen*0.5
                #system(sstr)
                sstr = sprintf( "play -r 48000 -n -b 32 -c 2 trim 0.0 %6.5f", totlen*0.5 )
                ctime += totlen*0.5
                #system(sstr)
            } else {
                sstr = sprintf( "play -r 48000 -n -b 32 -c 2 synth %6.5f sin %6.5f vol %6.5fdB", totlen*0.875, freqs[n], volume )
                ctime += totlen*0.875
                #system(sstr)
                sstr = sprintf( "play -r 48000 -n -b 32 -c 2 trim 0.0 %6.5f", totlen*0.125)
                ctime += totlen*0.125
                #system(sstr)
            }
            octaveup = 0
            octavedown = 0
            lastn = n
            crepl--
            } 
            crepl=1
        } 
    }
} END {
    printf( "ctime=%9.6f\n", ctime )
}

function abs(x) {
    if ( x < 0 ) {
        return( -x )
    } else {
        return( x )
    }
}
