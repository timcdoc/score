(part "Violin 1" part)  (subtitle "Allegro moderato" subtitle)
(clef TREBLE clef) (key 3b key) (timesig 4 4 timesig)
(dyn f dyn) (s 6 + e 5 b s) + g | 3 f (s e d s) e 5 e r e | 4 (s d c s) (s 5 c 3 c s) (s e d e s) f (s e d c s)
m4 | 5 (s c b s) 4 r (. e g b .) 3 | (s (g b g) 4. s) a 3 g (s (g g g) 4. s) f 3 e (s (g e g) 4. s) d 3 c (s (g c g) 4. s) b 3 a | 4 (s (g b g) 5 a s) g 4 r (. b e b .)
m7 | 3 (s c d s) (. e d .) (s c b s) (. a g .) (s f a s) (. c b .) (s a g s) (. f e .) | 4 + (. b - b b b .) 5 b 4 r 3 r + b | (dyn fp dyn) + 4 a (s f e s) d + (dyn fp dyn) b (s f e s) d
m10 | (dyn fp dyn) + b (s g f s) e (dyn fp dyn) e (s db c s) bn | (dyn f dyn) c (s e c a s) (s g b s) (s a f s) | (s 6 f 5 g s) 4 r 3 r b | (dyn fp dyn) + 4 a (s f e d s) + b (dyn fp dyn) (s f e d s) 
m14 | (dyn fp dyn) + b (s g f e s) e (s db c bn s) | (dyn f dyn) 3 (s c dn s) (. e c .) (s d e s) (. f d .) (s e f s) (. g e .) (s f g s) (. a f .) | (s g a s) (. b g .) (s a b s) (. c a .) 5 . b 4 e 
m17 | 3 (g d g) 4 c 3 d e d c b a 5 g f | (. 3 e d c b a g f e d e f g a f e d | e f g a b c d e d e f g a f e d .)
m20 | 5 e - 4 . e 3 e 5 e r | 7 r | (dyn p dyn) (sup "solo" sup) 4 r *7 g | (s b a s) 5 (s a 3 a s) (s c b c s) d (s c b a s)
 (ds m20 | 5 r - - 4 . g 3 g 5 g ds)
m24 | 4 (s a g s) 5 g - 4 b b b b | c c c c b + 5 f 4 f | 3 (s g f e f s) 5 e 4 (. e e e e .)
m27 | 5 e r 6 r | 4 r e e e c c c c | 5 b 4 r b b 5 b 4 e | 5 d 4 r b b 5 b 4 e
m31 | 5 d r 6 r | 4 *8 d | *8 c | *8 d | *8 c | 5 b r r + f
m37 | g r r g | 4 (. b b b b .) *4 an | 5 b r r (s f | g s) r r g | f r 6 r
m42 | 7 r | 7 r | 5 r (dyn p dyn) an an an | 6 b 5 r 4 r d | (s f e s) (s d c s) (s c b s) (s b an s) | 6 (s an 5 b s) r | r an an an
;page
m49 | 6 b 5 r 4 r d | (s (g f g) 5 e s) 4 (. d c .) 5 b (tr an tr) | 4 b r g r f r g r | f *5 b an an 
m53 | 5 b r b r | b r r - en | 4 f b b b *4 an | (dyn f dyn) (sup "Tutti" sup) 5 b + 6 b 3 (g an g) 4 (s g 3 f g s) | 5 (. f d e c .)
m58 | 5 b + 6 b 3 (g an g) 4 (s g 3 f g s) | 5 (. f - b c an | 3 b c d e f g an b an b c an + e c b an | b an g f e d c b an b c d e c b a .)
m62 | 5 b + b b r | 7 r | (dyn p dyn) (sup "solo" sup) 4 r - - *7 d | (s f e s) 5 e 3 (s d g f g s) (s an g f e s)
(ds m62 | 5 r d d ds)
(ds m62 | 5 r - f f ds)
m66 | 4 (s e d s) 5 d (. f f f f .) | g g g g (. f + c c c .) | 3 d (s c b c s) 5 b 4 b b b b | 5 (. b b b b 
m70 | b c .) - f 4 r b | (s (g 4 c ) 5 b s) an 6 r | 5 r - (s bn c d s) | 5 e 6 g 5 g | g r 6 r | 5 r - (s an b c s)
m76 | d 6 f 5 f | f r 6 r | 5 f 4 r (. f d f d f .) | 5 e 4 r e (. e g e g .) | 5 f 4 r f (. d f d f .)
m81 | 5 e 4 r b 5 e 4 r b | 5 e 4 r b 5 e 4 r c | b (. + b f d .) 5 b + b | (sup "Tutti" sup) (dyn f dyn) (s 6 e 5 b s) + (. g .) 
m85 | 3 f (s e d e s) 5 (. e .) r e | 4 (s d c s) 5 c 3 (s c e s) (. d e .) (s f e s) (. d c .) | 5 (s c b s) 4 r (. e g b .) | (s (g 3 b 4. a s) 3 g (s (g g g) 4. f s) 3 e (s (g e g) 4. d s) 3 c (s (g c g) 4. b s) 3 a | 5 g r 6 r
m90 | (sup "Solo" sup) (dyn p dyn) 4 r *7 g | (s b a s) 5 a 3 (s a c s) (. b c .) (s d c s) (. b a .) | 4 (s a g s) (. g g .) 5 g r | 4 (. *8 e .)
m94 | (. *5 e e db c b .) | an an an an + f f f f | (. f f f f .) f e d c | *4 bn + *12 g
m99 | a a g g f f f e e | (s d e s) (s d e s) (s d e s) (s d e s) | (s g b s) (s g b s) (s g b s) (s g b s) | (s d e s) (s d e s) (s d e s) (s d e s)
m103 | e + (. *3 e .) *8 e d d d d | (. e - e g b .) 5 - e r | 4 *8 g | *8 f |
m108 | *8 g | *8 f | 5 g r r (s b | c s) r r (s c | b s) r r (s b | c s) r r c
m114 | (. b e e e .) *16 e d d d d | e r c r b r c r | b r b r r (. b b b .)
m119 | c r c r r c c c | b (. e e e .) *4 d | b r b r r (. b b b .)
m122 | c r c r r c c c | b e e e *4 d | 5 e r e r | g r r - - an | 4 b + + (. e e e *4 d 
m127 | (sup "Tutti" sup) 3 e b c d e d e f g - d e f g f g a .) | 4 b 5 + f f f 4 f | 5 (dyn "cadenza" "fermata" dyn) r 6 r
m130 | (sup "Tutti" sup) (dyn f dyn) 5 e + 6 e (s 3 (g d g) s) 4 (s c 3 b c s) | 5 (. b g a f | e .) 6 + e (s 3 (g d g) s) 4 (s c 3 b c s) | 5 (. b - e f d .) | 3 e f g a b c d e d e f g a f e d
m135 | e d c b a g f e d e f g a f e d | 5 e 4 . e 3 e 5 e r
                               (ds m136 | 5 r - 4 . g 3 g 5 g ds)

